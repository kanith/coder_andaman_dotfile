export PATH="/usr/local/opt/python/libexec/bin:$PATH"
export PATH="$PATH:/usr/local/sbin"
alias mvn7='export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_80.jdk/Contents/Home && export JAVA_OPTS="-Dfile.encoding=UTF-8 -Xms1536m -Xmx3072m  -XX:PermSize=1024m -XX:MaxPermSize=2048m" && export MAVEN_OPTS="-Xmx1024m -XX:MaxPermSize=1024m" && mvn'
alias mvn7-ldd-leave='export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_80.jdk/Contents/Home && export JAVA_OPTS="-Dfile.encoding=UTF-8 -Xms1536m -Xmx3072m  -XX:PermSize=1024m -XX:MaxPermSize=2048m" && export MAVEN_OPTS="-Xmx1024m -XX:MaxPermSize=1024m" && mvn clean install tomcat7:run -P'

alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

export ORACLE_HOME=/opt/oracle/instantclient_12_2
export LD_RUN_PATH=/opt/oracle/instantclient_12_2:$LD_RUN_PATHi
